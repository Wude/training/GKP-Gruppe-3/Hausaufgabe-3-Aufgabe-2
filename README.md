# Hausaufgabe 3

## Aufgabe 2 - Geradengleichung (17 Punkte)

### Aufgabe 2.1 - Punkt und Line (4 Punkte)

Erstellen Sie mit Hilfe der in der Vorlesung kennengelernten
Verbünde (struct) Datentypen für Punkte und Linien im zweidimensionalen
Raum.

### Aufgabe 2.2 - Geradengleichung aus zwei Punkten ermitteln (4 Punkte)

Ziel ist die Berechnung einer Geraden durch zwei Punkte im R2 über die kartesische
Geradengleichung y = m  x + n. Sorgen Sie zunächst für eine Eingabe zweier Punkte
2
P1 und P2. (Hinweis: Sie benötigen für jeden Punkt eine x- und eine y-Koordinate.)
Ermitteln Sie nun die Parameter m (Anstieg) und n (Verschiebung) der Geradengleichung
und geben Sie dies aus.

### Aufgabe 2.3 - Mittelpunkt der Gerade (2 Punkte)

Stellen Sie sich einen Punkt P12 genau in der Mitte zwischen den Punkten P1 und P2 vor.
Erweitern Sie Ihr Programm so, dass es zunächst den x-Wert von P12 ermittelt und dann
daraus mittels der in Teil i) identifizierten Geradengleichung den dazugehörigen y-Wert
berechnet. Geben Sie die Punktkoordinaten von P12 aus.

### Aufgabe 2.4 - Beliebiger Punkt auf der Gerade (3 Punkte)

Fügen Sie Ihrem Algorithmus nun eine Schleife zur Eingabe eines beliebigen x-Wertes
hinzu, wonach der y-Wert des zugehörigen Punktes auf der Geraden ausgegeben werden
soll. Sorgen Sie durch eine geeignete Bedingung dafür, dass der Benutzer die Schleife
geordnet verlassen kann.

### Aufgabe 2.5 - Lot auf die Gerade (4 Punkte)

Erlauben Sie anschließend die Eingabe der Koordinaten eines Punktes P3 und lassen Sie
daraus die Parameter der Geradengleichung für das Lot von Punkt P3 auf die Gerade
durch P1 und P2 berechnen und ausgeben. Geben Sie darüber hinaus die Koordinaten
des Schnittpunkts der beiden Geraden (d. h. des Fußpunktes des Lotes) aus.
